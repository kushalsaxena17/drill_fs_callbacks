const fs = require("fs");
const path = require("path");
module.exports = function problem2(cb) {
	let deleteCount = 0;
	let fsRun = 0;
	let errorList = [];
	let outputList = [];

	fs.readFile("../lipsum.txt", "utf-8", (err, data) => {
		if (err) {
			errorList.push(err.message);
		} else {
			outputList.push("Reading Lipsum.txt file");
			let lipsumData = data.toString().toUpperCase();
			let newFileName = "lipsumUpperCased.txt";
			fs.writeFile(newFileName, lipsumData, (err) => {
				if (err) {
					errorList.push(err.message);
				} else {
					outputList.push("Written into lipsumUpperCase.txt");
					fs.writeFile("filenames.txt", newFileName, (err) => {
						if (err) {
							errorList.push(err.message);
						} else {
							outputList.push("Added to filenames.txt");

							fs.readFile(newFileName, "utf-8", (err, data) => {
								if (err) {
									errorList.push(err.message);
								} else {
									let newFileNameContent = data.toString().toLowerCase();
									let sentencedFile = newFileNameContent.split(". ").join("\n");
									let sentenceFileName = "sentencedLipsum.txt";
									fs.writeFile(sentenceFileName, sentencedFile, (err) => {
										if (err) {
											errorList.push(err.message);
										} else {
											outputList.push("Written into sentencedLipsum.txt");
											fs.appendFile("filenames.txt", "\n" + sentenceFileName, (err) => {
												if (err) {
													errorList.push(err.message);
												} else {
													fs.readFile(sentenceFileName, "utf-8", (err, data) => {
														if (err) {
															errorList.push(err.message);
														} else {
															let sentenedArr = data.split("\n");
															sentenedArr.sort((firstWord, secondWord) => {
																return firstWord.localeCompare(secondWord);
															}
															);
															let sentencedData = sentenedArr.join(" ");
															let sortedFileName = "sortedFileName.txt";
															fs.writeFile(sortedFileName, sentencedData, (err) => {
																if (err) {
																	errorList.push(err.message);
																}
																outputList.push("Written into sortedFileName.txt");
																fs.appendFile("filenames.txt", "\n" + sortedFileName, (err) => {
																	if (err) {
																		errorList.push(err.message);
																	} else {
																		outputList.push("SortedFileName.txt into filenames.txt");
																		fs.readFile("filenames.txt", "utf-8", (err, data) => {
																			if (err) {
																				errorList.push(err.message);
																			} else {
																				let fileNames = data.split("\n");
																				setTimeout(() => {
																					for (let index = 0; index < fileNames.length; index++) {
																						fs.unlink(fileNames[index], (err) => {
																							fsRun += 1;
																							if (err) {
																								errorList.push(err.message);
																							} else {
																								deleteCount += 1;
																								outputList.push("Done : " + fileNames[index]);
																								if (fsRun === 3 && deleteCount === 3) {

																									if (errorList.length === 0) {
																										cb(null, outputList);
																									} else {
																										cb(errorList, outputList);
																									}
																								}
																							}
																						}
																						);
																					}
																				}, 5000);
																			}
																		}
																		);
																	}
																}
																);
															}
															);
														}
													}
													);
												}
											}
											);
										}
									});
								}
							});
						}
					});
				}
			});
		}
	});
}


const fs = require("fs");
const path = require("path");
module.exports = function problem1(dirName, fileCount, cb) {
	if (typeof cb === "function") {
		try {
			let errorList = []
			let deleteCount = 0;
			fs.mkdir(dirName, { recursive: true }, (err) => {
				if (err) {
					errorList.push(Error("Wrong Data"));
				} else {
					cb(null, 'Created')
					for (let index = 0; index < fileCount; index++) {
						let filePath = path.join(__dirname, `./test/Random/${index}.json`);


						fs.writeFile(filePath, " ", function (err) {
							if (err) {
								errorList.push(Error("Something Wrong  making files"));
							} else {
								cb(null, "Created " + filePath);
								fs.unlink(filePath, (err) => {
									if (err) {
										errorList.push(Error("Wrong"));
									} else {
										deleteCount += 1;
										cb(null, "deleted " + filePath);
										if (deleteCount === fileCount) {
											cb(null, "Done");
										}
									}
								});
							}
						}
						);
					}
				}
			});
		} catch (err) {
			errorList.push(Error("Path Wrong"));
			cb(Error("Path Wrong"));
		}
	} else {
		errorList.push(Error("Not Function"));
		cb("Not Function");
	}
}

